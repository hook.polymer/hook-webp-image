// https://queryj.wordpress.com/2012/06/11/detecting-webp-support/
function testWebP(callback) {
  var webP = new Image();
  webP.src = 'data:image/webp;base64,UklGRi4AAABXRUJQVlA4TCEAAAAvAUAAEB8wAiMwAgSSNtse/cXjxyCCmrYNWPwmHRH9jwMA';
  webP.onload = webP.onerror = function () {
      callback(webP.height === 2);
  };
}

class WebPSupport {
  static init() {
    if(WebPSupport.initialized) {
      return;
    }
    WebPSupport.initialized = true;
    testWebP((supported) => {
      WebPSupport.isSupported = supported;
      WebPSupport.ready = true;
      WebPSupport.executeCallbacks();
    });
  }
  static supported(callback) {
    if(WebPSupport.ready) {
      callback(WebPSupport.isSupported);
      return;
    }
    WebPSupport.callbacks.push(callback);
  }
  static executeCallbacks() {
    if(!WebPSupport.ready) {
      return;
    }
    WebPSupport.callbacks.forEach((callback) => {
      callback(WebPSupport.supported);
    });
    WebPSupport.callbacks = [];
  }
}

WebPSupport.callbacks = [];
WebPSupport.ready = false;
WebPSupport.initialized = false;
WebPSupport.isSupported = false;
WebPSupport.init();

window.WebPSupport = WebPSupport;
